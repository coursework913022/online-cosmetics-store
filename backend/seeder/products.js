const products = [
    {
        name: "Палетка тіней для повік Catkin 9 Color",
        description:
            "Тіні Eyeshadow Palette C16, 13.5 г. Вони шовковистої текстури лягають рівномірним шаром, їх легко розтушовувати і нашаровувати, вони не осипаються. Завдяки водостійкій формулі макіяж зберігає первинний вигляд протягом дня незалежно від погоди.",
        count: 50,
        price: 450,
        category: "Очі",
        images: [
            { path: "/images/products/1-products.png" },
            { path: "/images/products/2-products.png" },
            { path: "/images/products/3-products.png" },
        ],
        rating: 5,
        reviewsNumber: 5,
        reviews: [],
        attrs: [{ key: "Товари для очей", value: "Тіні" }],
    },
    {
        name: "Консилер для обличчя NYX Professional Makeup",
        description:
            "Консилер NYX Professional Makeup Can not Stop Will not Stop Contour Concealer 08 True Beiгe 3,5 мл, рівномірно розподіляється по шкірі, ідеально маскує її недосконалості, а також допомагає затемнити або висвітлити певні області обличчя. Рекомендується використовувати разом з матувальним праймером Can't Stop Won't Stop, а також стійкою тональною основою Can't Stop Won't Stop від NYX.",
        count: 50,
        price: 266,
        category: "Обличчя",
        images: [
            { path: "/images/products/4-products.png" },
            { path: "/images/products/5-products.png" },
            { path: "/images/products/6-products.png" },
        ],
        rating: 4,
        reviewsNumber: 4,
        reviews: [],
        attrs: [{ key: "Товари для обличчя", value: "Консилери" }],
    },
    {
        name: "Матувальна компактна пудра для обличчя Maybelline",
        description:
            "Maybelline New York Fit Me! Matte + Poreless 104 Soft Ivory, 9 г. Приберіть блиск з обличчя та ефективно приховайте будь-які недоліки на шкірі. Саме з пудрою Fit Me Matte & Poreless ви з легкістю зможете досягти такого ефекту!",
        count: 50,
        price: 300,
        category: "Обличчя",
        images: [
            { path: "/images/products/7-products.png" },
            { path: "/images/products/8-products.png" },
            { path: "/images/products/9-products.png" },
        ],
        rating: 5,
        reviewsNumber: 5,
        reviews: [],
        attrs: [{ key: "Товари для обличчя", value: "Пудри" }],
    },
    {
        name: "Водостійкий олівець для губ Topface Waterproof Lipliner",
        description:
            "Водостійкий олівець для губ Topface Waterproof Lipliner 112, 1.14 г. Використовуйте можливості олівця наповну! Заштрихуйте ним усю поверхню губ, щоб створити стійку основу для помади. Використовуйте олівець як помаду або ж наносьте його на кутики губ, щоб візуально збільшити губи.",
        count: 50,
        price: 60,
        category: "Губи",
        images: [
            { path: "/images/products/10-products.png" },
            { path: "/images/products/11-products.png" },
            { path: "/images/products/12-products.png" },
        ],
        rating: 5,
        reviewsNumber: 5,
        reviews: [],
        attrs: [{ key: "Товари для губ", value: "Олівці" }],
    },
    {
        name: "Помада для губ Dior Rouge Dior Couture Colour Refillable",
        description:
            "Dior Lipstick 743 Rouge Zinnia, 3.5 г. Помада представлена в 16 відтінках з дзеркальним, сяйним або блискучим фінішем. Екстракт алое, а також доглядальні компоненти на основі натуральних олій і воску забезпечують губам до 24 годин зволоження.",
        count: 50,
        price: 2306,
        category: "Губи",
        images: [
            { path: "/images/products/13-products.png" },
            { path: "/images/products/14-products.png" },
            { path: "/images/products/15-products.png" },
        ],
        rating: 5,
        reviewsNumber: 5,
        reviews: [],
        attrs: [{ key: "Товари для губ", value: "Помади" }],
    },
    {
        name: "Матова помада для губ Guerlain KissKiss Tender Matte",
        description:
            "Guerlain KissKiss Tender Matte з ефектом сяяння, 910 Wanted Red, 2.8 г— це не просто помада. Це ніжний поцілунок з м’якістю оксамиту та мерехтливим сяйвом щастя на ваших губах. ",
        count: 50,
        price: 1699,
        category: "Губи",
        images: [
            { path: "/images/products/16-products.png" },
            { path: "/images/products/17-products.png" },
            { path: "/images/products/18-products.png" },
        ],
        rating: 5,
        reviewsNumber: 5,
        reviews: [],
        attrs: [{ key: "Товари для губ", value: "Помади" }],
    },
    {
        name: "Помада для губ L'Oreal Paris Color Riche 630 Beige a Nu",
        description:
            "Об'єм - 4.5 мл.Додайте в свій мейкап родзинку, скористайтеся технікою макіяжу губ «омбре»! Візьміть олівець для губ, який вигідно відтінятиме колір улюбленої помади, та профарбуйте контур. За допомогою аплікатора розтушуйте олівець до центру губ, а на решту поверхні нанесіть помаду.",
        count: 50,
        price: 499,
        category: "Губи",
        images: [
            { path: "/images/products/19-products.png" },
            { path: "/images/products/20-products.png" },
            { path: "/images/products/21-products.png" },
        ],
        rating: 5,
        reviewsNumber: 5,
        reviews: [],
        attrs: [{ key: "Товари для губ", value: "Помади" }],
    },
    {
        name: "Туш для брів Maybelline New York Brow Fast Sculpt",
        description:
            "Brow Fast Sculpt, 2.8 мл допомагає надати бровам натуральної форми й забезпечити природне та стійке фарбування. Формула туші містить спеціальний пігмент, що дає змогу просто та швидко нафарбувати брови. ",
        count: 50,
        price: 499,
        category: "Брови",
        images: [
            { path: "/images/products/22-products.png" },
            { path: "/images/products/23-products.png" },
            { path: "/images/products/24-products.png" },
        ],
        rating: 5,
        reviewsNumber: 7,
        reviews: [],
        attrs: [{ key: "Товари для брів", value: "Туші для брів" }],
    },
    {
        name: "Туш для брів L'Oreal Paris Brow Plump & Set Plumper",
        description:
            "Для фіксації форми та додання кольору 108 Темний брюнет, 4.9 мл. Акуратні й доглянуті брови тепер легко створити одним порухом руки. Відомий французький косметичний бренд L'Oreal Paris Brow пропонує перезапуск туші для брів Brow Artist Plumper для фіксації форми й додання кольору. ",
        count: 50,
        price: 400,
        category: "Брови",
        images: [
            { path: "/images/products/25-products.png" },
            { path: "/images/products/26-products.png" },
            { path: "/images/products/27-products.png" },
        ],
        rating: 4,
        reviewsNumber: 15,
        reviews: [],
        attrs: [{ key: "Товари для брів", value: "Туші для брів" }],
    },
    {
        name: "Помада для брів Avenir Cosmetics Eeybrow Gel Taupe, 2.5 г",
        description:
            "Малюйте короткі штрихи та імітуйте волоски для візуального збільшення об’єму й густини брів. Починайте промальовувати брови з точки зламу, щоб проконтролювати інтенсивність і створити натуральний відтінковий перехід. Не забудьте про ретельне розчісування та розтушовку в кінці.",
        count: 50,
        price: 81,
        category: "Брови",
        images: [
            { path: "/images/products/28-products.png" },
            { path: "/images/products/29-products.png" },
            { path: "/images/products/30-products.png" },
        ],
        rating: 3,
        reviewsNumber: 6,
        reviews: [],
        attrs: [{ key: "Товари для брів", value: "Помадки для брів" }],
    },
];

module.exports = products;