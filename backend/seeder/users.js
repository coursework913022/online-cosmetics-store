const bcrypt = require("bcryptjs")

const users = [
      {
    name: 'admin',
    lastName: 'admin',
    email: 'admin@admin.com',
    password: bcrypt.hashSync('admin@admin.com', 10),
    isAdmin: true,
  },
  {
    name: 'Victoria',
    lastName: 'Havryshchuk',
    email: 'vika70147@gmail.com',
    password: bcrypt.hashSync('vika70147@gmail.com', 10),
  },
]

module.exports = users
