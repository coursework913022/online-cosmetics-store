const ObjectId = require("mongodb").ObjectId

const reviews = [
    {
        comment: "Краса! Я люблю це!",
        rating: 5,
        user: { _id: ObjectId(), name: "Victoria" },
    },
    {
        comment: "Щойно забрала, все шик",
        rating: 5,
        user: { _id: ObjectId(), name: "Karina P" },
    },
    {
        comment: "дякую",
        rating: 5,
        user: { _id: ObjectId(), name: "Vlad" },
    },
    {
        comment: "Майже ідеально",
        rating: 4,
        user: { _id: ObjectId(), name: "Alexander" },
    },
    {
        comment: "Під час пересилки товар пошкодили!",
        rating: 2,
        user: { _id: ObjectId(), name: "AlexPro" },
    },
]

module.exports = reviews
