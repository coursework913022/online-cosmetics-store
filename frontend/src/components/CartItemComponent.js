import { Row, Col, Image, ListGroup, Form, Button } from "react-bootstrap";


const CartItemComponent = () => {
    return (
        <>
            <ListGroup.Item>
                <Row>
                    <Col md={2}>
                        <Image crossOrigin="anonymous" fluid
                            src="/images/products/1-products.png" />
                    </Col>
                    <Col md={2}>
                    Палетка тіней для повік Catkin 9 Color
                    </Col>
                    <Col md={2}>
                        <b>450 грн.</b>
                    </Col>
                    <Col md={3}>
                        <Form.Select>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </Form.Select>
                    </Col>
                    <Col md={3}>
                        <Button
                            type="button"
                            variant="secondary"
                            onClick={() => window.confirm("Are you sure?")}
                        >
                            <i className="bi bi-trash"></i>
                        </Button>
                    </Col>
                </Row>
            </ListGroup.Item>
            <br />
        </>
    )
}

export default CartItemComponent;