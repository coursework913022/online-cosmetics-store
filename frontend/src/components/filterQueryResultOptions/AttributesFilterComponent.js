import { Form } from "react-bootstrap";

const AttributesFilterComponent = () => {
  return (
    <>
      {[{ Класи: ["Масс маркет", "Люксова", "Професійна"] }, { Призначення: ["Гіпоалергенна ", "Звичайна"] }].map((item, idx) => (
        <div key={idx} className="mb-3">
          <Form.Label><b>{Object.keys(item)}</b></Form.Label>
          {item[Object.keys(item)].map((i, idx) => (
            <Form.Check
              type="checkbox"
              key={idx}
              id="default-checkbox"
              label={i}
            />
          ))}
        </div>
      ))}
    </>
  );
};

export default AttributesFilterComponent;
