import { Button, Card } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const CategoryCardComponent = ({ category, idx }) => {
    const images = [
        "/images/Очі-category.png",
        "/images/Брови-category.png",
        "/images/Губи-category.png",
        "/images/Обличчя-category.png",
    ]
    return (
        <Card>
            <Card.Img crossOrigin="anonymous" variant="top" src={images[idx]} />
            <Card.Body>
                <Card.Title>{category}</Card.Title>
                <Card.Text>
                    Натиснувши перейти ви змножете обирати товари саме цієї категорії.
                </Card.Text>
                <LinkContainer to="/product-list">
                    <Button variant="primary">Перейти</Button>
                </LinkContainer>
            </Card.Body>
        </Card>
    )
}

export default CategoryCardComponent;