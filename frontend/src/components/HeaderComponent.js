
import { Container, Nav, Navbar, NavDropdown, Badge, Form, Dropdown, DropdownButton, Button, InputGroup } from "react-bootstrap";

import { LinkContainer } from "react-router-bootstrap";

import { Link } from "react-router-dom";

const HeaderComponent = () => {
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <LinkContainer to="/">
                    <Navbar.Brand href="/">GAVI MAKEUP</Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <InputGroup>
                            {/* search, filter */}
                            <DropdownButton id="dropdown-basic-button" title="Усі">
                                <Dropdown.Item >Action</Dropdown.Item>
                                <Dropdown.Item >Action</Dropdown.Item>
                                <Dropdown.Item >Action</Dropdown.Item>
                                <Dropdown.Item >Action</Dropdown.Item>
                            </DropdownButton>

                            <Form.Control type="text" placeholder="Пошук.." />

                            <Button variant="warning">
                                <i className="bi bi-search text-dark"></i>
                            </Button>
                            {/* /search, filter */}
                        </InputGroup>
                    </Nav>
                    <Nav>
                        <LinkContainer to="/admin/orders">
                            <Nav.Link>
                                Адмін
                                <span className="position-absolute top-1 start-10 translate-middle 
                                p-2 bg-danger border border-light rounded-circle"></span>
                            </Nav.Link>
                        </LinkContainer>


                        <NavDropdown title="Vika Havryshchuk" id="collasible-nav-dropdown">
                            <NavDropdown.Item eventKey="/user/my-orders" as={Link} to="/user/my-orders">Мої замовлення</NavDropdown.Item>
                            <NavDropdown.Item eventKey="/user" as={Link} to="/user">Мій профіль</NavDropdown.Item>
                            <NavDropdown.Item>Вийти</NavDropdown.Item>
                        </NavDropdown>

                        <LinkContainer to="/login">
                            <Nav.Link>
                                Вхід
                            </Nav.Link>
                        </LinkContainer>

                        <LinkContainer to="/register">
                            <Nav.Link>
                                Регістрація
                            </Nav.Link>
                        </LinkContainer>

                        <LinkContainer to="/cart">
                            <Nav.Link>
                                <Badge pill bg="danger">
                                    2
                                </Badge>
                                <i className="bi bi-cart-dash"></i>
                                <span className="ms-1">Кошик</span>
                            </Nav.Link>
                        </LinkContainer>
                    </Nav>

                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
};

export default HeaderComponent;