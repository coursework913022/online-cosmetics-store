import { Form } from "react-bootstrap";

const SortOptionsComponent = () => {
  return (
    <Form.Select aria-label="Default select example">
      <option>Сортувати за</option>
      <option value="price_1">Ціна: Від нижчої до вищої</option>
      <option value="price_-1">Ціна: Від вищої до нижчої</option>
      <option value="rating_-1">За рейтингом</option>
      <option value="name_1">Назвою А-Я</option>
      <option value="name_-1">Назвою Я-А</option>
    </Form.Select>
  );
};

export default SortOptionsComponent;

