import { Carousel } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const ProductCarouselComponent = () => {
    const cursorP = { cursor: "pointer" };
    return (
        <Carousel>
            <Carousel.Item>
                <LinkContainer to="/product-details" style={cursorP}>
                    <img
                        crossOrigin="anonymous"
                        className="d-block w-100"
                        style={{ height: "500", objectFit: "cover" }}
                        src="images/carousel/slide_1.png"
                        alt="First slide"
                    />
                </LinkContainer>
            </Carousel.Item>
            <Carousel.Item>
                <LinkContainer to="/product-details" style={cursorP}>
                    <img
                        crossOrigin="anonymous"
                        className="d-block w-100"
                        style={{ height: "500", objectFit: "cover" }}
                        src="images/carousel/slide_2.png"
                        alt="Second slide"
                    />
                </LinkContainer>
            </Carousel.Item>
            <Carousel.Item>
                <LinkContainer to="/product-details" style={cursorP}>
                    <img
                        crossOrigin="anonymous"
                        className="d-block w-100"
                        style={{ height: "500", objectFit: "cover" }}
                        src="images/carousel/slide_3.png"
                        alt="Third slide"
                    />
                </LinkContainer>
                {/* <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>
                        Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                    </p>
                </Carousel.Caption> */}
            </Carousel.Item>
        </Carousel>
    )
}

export default ProductCarouselComponent;