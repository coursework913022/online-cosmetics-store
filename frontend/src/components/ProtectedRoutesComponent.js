import { Outlet, Navigate } from "react-router-dom";
import UserChatComponent from "./user/UserChatComponent";

// if user is not utentification, we redirect to the login page
const ProtectedRoutesComponent = ({ admin }) => {
    if (admin) {
        let adminAuth = true;
        return adminAuth ? <Outlet /> : <Navigate to="/login" />;
    } else {
        let userAuth = true;
        return userAuth ? <> <UserChatComponent /> <Outlet /> </> : <Navigate to="/login" />;
    }
};

export default ProtectedRoutesComponent;