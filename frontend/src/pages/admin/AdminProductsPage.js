
import { Row, Col, Table, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import AdminLinksComponent from "../../components/admin/AdminLinksComponent";

const deleteHandler = () => {
    if (window.confirm("Ви впевнені, що хочете видалити?")) alert("Товар видалено!");
}

const AdminProductsPage = () => {
    return (
        <Row className="m-5">
            <Col md={2}>
                <AdminLinksComponent />
            </Col>
            <Col md={10}>
                <h1>
                    Список продуктів {" "}
                    <LinkContainer to="/admin/create-new-product">
                        <Button variant="primary" size="lg">
                            Створити новий
                        </Button>
                    </LinkContainer>

                </h1>
                <Table striped bordered hover responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Назва товару</th>
                            <th>Ціна</th>
                            <th>Категорія</th>
                            <th>Відредагувати/Видалити</th>
                        </tr>
                    </thead>
                    <tbody>
                        {[{ name: "Хайлайтер для обличчя Makeup Revolution Glass Illuminator", price: "350", category: "Хайлайтери" },
                        { name: "Матувальна компактна пудра для обличчя Maybelline New York Fit Me! Matte + Poreless 104 Soft Ivory", price: "300", category: "Пудри" },
                        { name: "Розсипчаста пудра для обличчя AA Wings of Color Dust Matt Loose Powder 31 Skin Fresher", price: "578", category: "Пудри" }].map(
                            (item, idx) => (
                                <tr key={idx}>
                                    <td>{idx + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.price}</td>
                                    <td>{item.category}</td>
                                    <td>
                                        <LinkContainer to="/admin/edit-product">
                                            <Button className="btn-small">
                                                <i className="bi bi-pencil-square"></i>
                                            </Button>
                                        </LinkContainer>
                                        {" / "}
                                        <Button variant="danger" className="btn-small" onClick={deleteHandler}>
                                            <i className="bi bi-trash"></i>
                                        </Button>
                                    </td>
                                </tr>
                            )
                        )}
                    </tbody>
                </Table>
            </Col>
        </Row>
    );
};


export default AdminProductsPage

