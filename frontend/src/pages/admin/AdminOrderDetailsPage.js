
import {
    Container,
    Row,
    Col,
    Form,
    Alert,
    ListGroup,
    Button,
} from "react-bootstrap";
import CartItemComponent from "../../components/CartItemComponent";

const AdminOrderDetailsPage = () => {
    return (
        <Container fluid>
            <Row className="mt-4">
                <h1>Деталі замовлення</h1>
                <Col md={8}>
                    <br />
                    <Row>
                        <Col md={6}>
                            <h2>Доставка</h2>
                            <b>Ім'я</b>: John Doe <br />
                            <b>Адреса</b>: 8739 Mayflower St. Los Angeles, CA 90063 <br />
                            <b>Телефон</b>: 888 777 666
                        </Col>
                        <Col md={6}>
                            <h2>Метод оплати</h2>
                            <Form.Select disabled={false}>
                                <option value="pp">PayPal</option>
                                <option value="cod">
                                    Оплата при отриманні
                                </option>
                            </Form.Select>
                        </Col>
                        <Row>
                            <Col>
                                <Alert className="mt-3" variant="danger">
                                    Не доставлено
                                </Alert>
                            </Col>
                            <Col>
                                <Alert className="mt-3" variant="success">
                                    Сплачено 2022-10-02
                                </Alert>
                            </Col>
                        </Row>
                    </Row>
                    <br />
                    <h2>Замовлені товари</h2>
                    <ListGroup variant="flush">
                        {Array.from({ length: 3 }).map((item, idx) => (
                            <CartItemComponent key={idx} />
                        ))}
                    </ListGroup>
                </Col>
                <Col md={4}>
                    <ListGroup>
                        <ListGroup.Item>
                            <h3>Підсумок замовлення</h3>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Ціна товару (після сплати податків): <span className="fw-bold">$892</span>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Доставка: <span className="fw-bold">included</span>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Податок: <span className="fw-bold">included</span>
                        </ListGroup.Item>
                        <ListGroup.Item className="text-danger">
                            Загальна вартість: <span className="fw-bold">$904</span>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <div className="d-grid gap-2">
                                <Button size="lg" variant="danger" type="button">
                                    Позначити як доставлене
                                </Button>
                            </div>
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        </Container>
    );
};


export default AdminOrderDetailsPage

