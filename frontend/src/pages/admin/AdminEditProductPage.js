import {
    Row,
    Col,
    Container,
    Form,
    Button,
    CloseButton,
    Table,
    Alert,
    Image
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState } from "react";

const onHover = {
    cursor: "pointer",
    position: "absolute",
    left: "5px",
    top: "-10px",
    transform: "scale(2.7)",
}

const AdminEditProductPage = () => {
    const [validated, setValidated] = useState(false);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);
    };

    return (
        <Container>
            <Row className="justify-content-md-center mt-5">
                <Col md={2}>
                    <Link to="/admin/products" className="btn btn-dark my-2 mx-5">
                        Повернутись
                    </Link>
                </Col>
                <Col md={6}>
                    <h1>Редагувати товар</h1>
                    <Form noValidate validated={validated} onSubmit={handleSubmit}>
                        {/* name */}
                        <Form.Group className="mb-3" controlId="formBasicName">
                            <Form.Label>Назва</Form.Label>
                            <Form.Control name="name" required type="text"
                                defaultValue="Product" />
                        </Form.Group>

                        {/* description */}
                        <Form.Group
                            className="mb-3"
                            controlId="exampleForm.ControlTextarea1"
                        >
                            <Form.Label>Опис</Form.Label>
                            <Form.Control
                                name="description"
                                required
                                as="textarea"
                                rows={3}
                                defaultValue="Product Description"
                            />
                        </Form.Group>

                        {/* count */}
                        <Form.Group className="mb-3" controlId="formBasicCount">
                            <Form.Label>Кількість в наявності</Form.Label>
                            <Form.Control name="count" required type="number"
                                defaultValue="2" />
                        </Form.Group>

                        {/* price */}
                        <Form.Group className="mb-3" controlId="formBasicPrice">
                            <Form.Label>Ціна</Form.Label>
                            <Form.Control name="price" required type="text"
                                defaultValue="100" />
                        </Form.Group>

                        {/* Choose/create category */}
                        <Form.Group className="mb-3" controlId="formBasicCategory">
                            <Form.Label>Категорія</Form.Label>
                            <Form.Select
                                required
                                name="category"
                                aria-label="Default select example"
                            >
                                <option value="">Оберіть категорію</option>
                                <option value="1">Очі</option>
                                <option value="2">Брови</option>
                                <option value="3">Губи</option>
                                <option value="3">Обличчя</option>
                            </Form.Select>
                        </Form.Group>

                        {/* Choose/create attribute */}
                        <Row className="mt-5">
                            <Col md={6}>
                                <Form.Group className="mb-3" controlId="formBasicAttributes">
                                    <Form.Label>Виберіть атрибут і встановіть значення</Form.Label>
                                    <Form.Select
                                        name="atrrKey"
                                        aria-label="Default select example"
                                    >
                                        <option>Виберіть атрибут</option>
                                        <option value="red">колір</option>
                                    </Form.Select>
                                </Form.Group>
                            </Col>
                            <Col md={6}>
                                <Form.Group
                                    className="mb-3"
                                    controlId="formBasicAttributeValue"
                                >
                                    <Form.Label>Значення атрибуту</Form.Label>
                                    <Form.Select
                                        name="atrrVal"
                                        aria-label="Default select example"
                                    >
                                        <option>Виберіть значення атрибута</option>
                                    </Form.Select>
                                </Form.Group>
                            </Col>
                        </Row>

                        <Row>
                            <Table hover>
                                <thead>
                                    <tr>
                                        <th>Атрибут</th>
                                        <th>Значення</th>
                                        <th>Видалити</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>ключ атрибута</td>
                                        <td>значення атрибута</td>
                                        <td>
                                            <CloseButton />
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Row>

                        <Row>
                            <Col md={6}>
                                <Form.Group className="mb-3" controlId="formBasicNewAttribute">
                                    <Form.Label>Створити новий атрибут</Form.Label>
                                    <Form.Control
                                        disabled={false}
                                        placeholder="спочатку виберіть або створіть категорію"
                                        name="newAttrValue"
                                        type="text"
                                    />
                                </Form.Group>
                            </Col>
                            <Col md={6}>
                                <Form.Group
                                    className="mb-3"
                                    controlId="formBasicNewAttributeValue"
                                >
                                    <Form.Label>Значення атрибуту</Form.Label>
                                    <Form.Control
                                        disabled={false}
                                        placeholder="спочатку виберіть або створіть категорію"
                                        required={true}
                                        name="newAttrValue"
                                        type="text"
                                    />
                                </Form.Group>
                            </Col>
                        </Row>

                        <Alert variant="primary">
                            Після введення ключа атрибута і значення натисніть <b>Enter</b> на одному з полів
                        </Alert>

                        {/* images */}
                        <Form.Group controlId="formFileMultiple" className="mb-3 mt-3">
                            <Form.Label>Зображення</Form.Label>
                            <Row>
                                <Col style={{ position: "relative" }} xs={3}>
                                    <Image crossOrigin="anonymous" fluid src="/images/tablets-category.png" />
                                    <i className="bi bi-x text-danger" style={onHover}></i>
                                </Col>
                                <Col style={{ position: "relative" }} xs={3}>
                                    <Image crossOrigin="anonymous" fluid src="/images/tablets-category.png" />
                                    <i className="bi bi-x text-danger" style={onHover}></i>
                                </Col>
                            </Row>
                            <Form.Control required type="file" multiple />
                        </Form.Group>


                        <Button variant="primary" type="submit">
                            Зберегти зміни
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default AdminEditProductPage

