
import { Row, Col, Table, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import AdminLinksComponent from "../../components/admin/AdminLinksComponent";

const deleteHandler = () => {
    if (window.confirm("Ви впевнені, що хочете видалити?")) alert("Користувача видалено");
}

const AdminUserPage = () => {
    return (
        <Row className="m-5">
            <Col md={2}>
                <AdminLinksComponent />
            </Col>
            <Col md={10}>
                <h1>Список користувачів</h1>
                <Table striped bordered hover responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ім'я</th>
                            <th>Прізвище</th>
                            <th>Емейл</th>
                            <th>Права адміністратора</th>
                            <th>Редагувати/Видалити</th>
                        </tr>
                    </thead>
                    <tbody>
                        {["bi bi-check-lg text-success", "bi bi-x-lg text-danger"].map(
                            (item, idx) => (
                                <tr key={idx}>
                                    <td>{idx + 1}</td>
                                    <td>Mark</td>
                                    <td>Twain</td>
                                    <td>email@email.com</td>
                                    <td>
                                        <i className={item}></i>
                                    </td>
                                    <td>
                                        <LinkContainer to="/admin/edit-user">
                                            <Button className="btn-sm">
                                                <i className="bi bi-pencil-square"></i>
                                            </Button>
                                        </LinkContainer>
                                        {" / "}
                                        <Button variant="danger" className="btn-sm" onClick={deleteHandler}>
                                            <i className="bi bi-x-square"></i>
                                        </Button>
                                    </td>
                                </tr>
                            )
                        )}
                    </tbody>
                </Table>
            </Col>
        </Row>
    );
};

export default AdminUserPage

