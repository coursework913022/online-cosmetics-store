
import {
    Row,
    Col,
    Container,
    Form,
    Button,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState } from "react";


const AdminEditUserPage = () => {
    const [validated, setValidated] = useState(false);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);
    };

    return (
        <Container>
            <Row className="justify-content-md-center mt-5">
                <Col md={2}>
                    <Link to="/admin/users" className="btn btn-dark my-2 mx-5">
                        Повернутись
                    </Link>
                </Col>
                <Col md={6}>
                    <h1>Редагувати користувача</h1>
                    <Form noValidate validated={validated} onSubmit={handleSubmit}>
                        {/* name */}
                        <Form.Group className="mb-3" controlId="formBasicFirstName">
                            <Form.Label>Ім'я</Form.Label>
                            <Form.Control name="name" required type="text" defaultValue="Ім'я" />
                        </Form.Group>

                        {/* description */}
                        <Form.Group
                            className="mb-3"
                            controlId="formBasicLastName"
                        >
                            <Form.Label>Прізвище</Form.Label>
                            <Form.Control
                                name="lastName"
                                required
                                type="text"
                                defaultValue="Прізвище"
                            />
                        </Form.Group>

                        {/* email */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Емейл</Form.Label>
                            <Form.Control name="email" required type="email"
                                defaultValue="email@email.com" />
                        </Form.Group>

                        {/* checkbox */}
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Адмін" name="isAdmin" />
                        </Form.Group>

                        <Button variant="primary" type="submit">
                            Зберегти зміни
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}


export default AdminEditUserPage

