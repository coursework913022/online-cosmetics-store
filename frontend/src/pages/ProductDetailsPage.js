import {
    Row,
    Col,
    Container,
    Image,
    ListGroup,
    Form,
    Button,
    Alert,
    Carousel,
} from "react-bootstrap";
import { Rating } from "react-simple-star-rating";
import AddedToCartMessageComponent from "../components/AddedToCartMessageComponent";

import ImageZoom from "js-image-zoom";
import { useEffect } from "react";

const ProductDetailsPage = () => {
    var options = {
        // width: 400,
        // zoomWidth: 500,
        // fillContainer: true,
        // zoomPosition: "bottom",
        height: 600,
        scale: 2,
        offset: { vertical: 0, horizontal: 0 },
    };

    useEffect(() => {
        new ImageZoom(document.getElementById("first"), options);
        new ImageZoom(document.getElementById("second"), options);
        new ImageZoom(document.getElementById("third"), options);
        new ImageZoom(document.getElementById("fourth"), options);
    });

    return (
        <Container>
            <AddedToCartMessageComponent />
            <Row className="mt-5">
                <Col style={{ zIndex: 1 }} md={5}>
                    {/* Слайдер з зображеннями товарів */}
                    <Carousel >
                        <Carousel.Item>
                            <div id="first">
                                <Image crossOrigin="anonymous" fluid src="/images/products/1-products.png" />
                            </div>
                        </Carousel.Item>

                        <Carousel.Item>
                            <div id="second">
                                <Image crossOrigin="anonymous" fluid src="/images/products/2-products.png" />
                            </div>
                        </Carousel.Item>

                        <Carousel.Item>
                            <div id="third">
                                <Image crossOrigin="anonymous" fluid src="/images/products/3-products.png" />
                            </div>
                        </Carousel.Item>
                    </Carousel>
                    {/* /Слайдер з зображеннями товарів */}
                </Col>

                <Col md={7}>
                    <Row>
                        <Col md={8}>
                            <ListGroup variant="flush">
                                <ListGroup.Item><h1>Консилер для обличчя NYX Professional Makeup</h1></ListGroup.Item>
                                <ListGroup.Item>
                                    <Rating readonly size={20} initialValue={4} /> (1)
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    Ціна
                                    <span className="fw-bold"> 133 грн.</span>
                                </ListGroup.Item>
                                <ListGroup.Item>
                                Консилер NYX Professional Makeup Can not Stop Will not Stop Contour Concealer 08 True Beiгe 3,5 мл, рівномірно розподіляється по шкірі, ідеально маскує її недосконалості, а також допомагає затемнити або висвітлити певні області обличчя. Рекомендується використовувати разом з матувальним праймером Can't Stop Won't Stop, а також стійкою тональною основою Can't Stop Won't Stop від NYX.
                                </ListGroup.Item>
                            </ListGroup>
                        </Col>
                        <Col md={4}>
                            <ListGroup>
                                <ListGroup.Item>Статус: наяне</ListGroup.Item>
                                <ListGroup.Item>
                                    Ціна: <span className="fw-bold"> 266 грн.</span>
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    Кількість:
                                    <Form.Select size="lg" aria-label="Default select example">
                                        <option>1</option>
                                        <option value="1">2</option>
                                        <option value="2">3</option>
                                        <option value="3">4</option>
                                    </Form.Select>
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <Button variant="danger">Додати до корзини</Button>
                                </ListGroup.Item>
                            </ListGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="mt-5">
                            <h5>Відгуки:</h5>
                            <ListGroup variant="flush">
                                {Array.from({ length: 1 }).map((item, idx) => (
                                    <ListGroup.Item key={idx}>
                                        Vicoria
                                        <br />
                                        <Rating readonly size={20}
                                            initialValue={4} />
                                        <br />
                                        15-06-2023
                                        <br />
                                        Краса! Я люблю це!
                                    </ListGroup.Item>
                                ))}
                            </ListGroup>
                        </Col>
                    </Row>
                    <hr />
                    <Alert variant="danger">Спочатку увійдіть для того щоб написати відгук.</Alert>
                    <Form>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">

                            <Form.Label>Напишіть відгук</Form.Label>
                            <Form.Control as="textarea" rows={3} />
                        </Form.Group>
                        <Form.Select aria-label="Default select example">
                            <option>Ваша оцінка</option>
                            <option value="5">5 (дуже добре)</option>
                            <option value="4">4 (добре)</option>
                            <option value="3">3 (нормально)</option>
                            <option value="2">2 (погано)</option>
                            <option value="1">1 (жахливо)</option>
                        </Form.Select>
                        <Button className="mb-3 mt-3" variant="primary">Надіслати</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default ProductDetailsPage;

