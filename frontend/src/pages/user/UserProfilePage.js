
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import { useState } from "react";

const UserProfilePage = () => {
    const [validated, setValidated] = useState(false);

    const onChange = () => {
        const password = document.querySelector("input[name=password]")
        const confirm = document.querySelector("input[name=confirmPassword]")

        if (confirm.value === password.value) {
            confirm.setCustomValidity("")
        } else {
            confirm.setCustomValidity("Passwords do not match")
        }
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);
    };
    return (
        <Container>
            <Row className="mt-5 justify-content-md-center">
                <Col md={6}>
                    <h1>Профіль користувача</h1>
                    <Form noValidate validated={validated} onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="validationCustom01">
                            <Form.Label>Ваше ім'я</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                defaultValue="Default"
                                name="name"
                            />
                            <Form.Control.Feedback type="invalid">
                                Будь ласка введіть ваше ім'я
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicLastName">
                            <Form.Label>Ваше прізвище</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                defaultValue="Default"
                                name="lastName"
                            />
                            <Form.Control.Feedback type="invalid">
                                Будь ласка введіть ваше прізвище
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Емейл адреса</Form.Label>
                            <Form.Control
                                disabled
                                name="email"
                                type="email"
                                defaultValue="Емейл адреса не може бути зміненою, лише шляхом видалення акаунту та реєстрації з новим."
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPhone">
                            <Form.Label>Номер телефону</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Введіть ваш номер телефону"
                                defaultValue=""
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicAddress">
                            <Form.Label>Адреса</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Введіть назву вашої вулиці та номер будинку"
                                defaultValue=""
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCountry">
                            <Form.Label>Країна</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Введіть назву вашої країни"
                                defaultValue=""
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicZip">
                            <Form.Label>Поштовий індекс</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Введіть ваш поштовий індекс"
                                defaultValue=""
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCity">
                        <Form.Label>Місто</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Введіть назву вашого міста"
                                defaultValue=""
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicState">
                        <Form.Label>Держава</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Введіть назву вашої держави"
                                defaultValue=""
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control
                                name="password"
                                required
                                type="password"
                                placeholder="Пароль"
                                minLength={6}
                                onChange={onChange}
                            />
                            <Form.Control.Feedback type="invalid">
                                Будь ласка введіть пароль
                            </Form.Control.Feedback>
                            <Form.Text className="text-muted">
                                Пароль повинен складатися як мінімум з 6 символів!
                            </Form.Text>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPasswordRepeat">
                            <Form.Label>Повторіть пароль</Form.Label>
                            <Form.Control
                                name="confirmPassword"
                                required
                                type="password"
                                placeholder="Повторіть пароль"
                                minLength={6}
                                onChange={onChange}
                            />
                            <Form.Control.Feedback type="invalid">
                                Обидва паролі повинні співпадати
                            </Form.Control.Feedback>
                        </Form.Group>

                        <Button variant="primary" type="submit">
                            Оновити
                        </Button>
                        <Alert show={true} variant="danger">
                            Користувач з таким емейлом вже існує!
                        </Alert>
                        <Alert show={true} variant="info">
                            Дані користувача оновлено
                        </Alert>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};


export default UserProfilePage

