
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import { useState } from "react";
import { Link } from "react-router-dom";
import Spinner from 'react-bootstrap/Spinner'

const LoginPage = () => {
    const [validated, setValidated] = useState(false);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);
    };
    return (
        <Container>
            <Row className="mt-5 justify-content-md-center">
                <Col md={6}>
                    <h1>Вхід</h1>
                    <Form noValidate validated={validated} onSubmit={handleSubmit}>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Емейл адреса</Form.Label>
                            <Form.Control
                                name="email"
                                required
                                type="email"
                                placeholder="Введіть емейл адресу"
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control
                                name="password"
                                required
                                type="password"
                                placeholder="Пароль"
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check
                                name="doNotLogout"
                                type="checkbox"
                                label="Не вийшов"
                            />
                        </Form.Group>

                        <Row className="pb-2">
                            <Col>
                                Ви ще не зареєстровані?
                                <Link to={"/register"}> Регістрація </Link>
                            </Col>
                        </Row>

                        <Button variant="primary" type="submit">
                            <Spinner
                                as="span"
                                animation="border"
                                size="sm"
                                role="status"
                                aria-hidden="true"
                            />
                            Увійти
                        </Button>
                        <Alert show={true} variant="danger">
                            Такого корисувача не існує!
                        </Alert>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default LoginPage