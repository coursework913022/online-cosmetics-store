import ProductCarouselComponent from "../components/ProductCarouselComponent";
import CategoryCardComponent from "../components/CategoryCardComponent";
import { Row, Container } from "react-bootstrap";

const HomePage = () => {
    const categories = [
        "Товари для очей",
        "Товари для брів",
        "Товари для губ",
        "Товари для обличчя",
    ]

    return (
        <>
            <ProductCarouselComponent />
            <Container>
                <Row xs={2} md={4} className="g-4 mt-5">
                    {
                        categories.map((category, idx) => (
                            <CategoryCardComponent key={idx} category={category} idx={idx} />
                        ))}
                </Row>
            </Container>
        </>
    )
}

export default HomePage