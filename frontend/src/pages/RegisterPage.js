import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import { useState } from "react";
import { Link } from "react-router-dom";
import Spinner from 'react-bootstrap/Spinner'

const RegisterPage = () => {
    const [validated, setValidated] = useState(false);

    const onChange = () => {
        const password = document.querySelector("input[name=password]")
        const confirm = document.querySelector("input[name=confirmPassword]")

        if (confirm.value === password.value) {
            confirm.setCustomValidity("")
        } else {
            confirm.setCustomValidity("Passwords do not match")
        }
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);
    };
    return (
        <Container>
            <Row className="mt-5 justify-content-md-center">
                <Col md={6}>
                    <h1>Реєстрація</h1>
                    <Form noValidate validated={validated} onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="validationCustom01">
                            <Form.Label>Ваше ім'я</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="Введіть ваше ім'я"
                                name="name"
                            />
                            <Form.Control.Feedback type="invalid">
                                Будь ласка введіть ваше ім'я
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicLastName">
                            <Form.Label>Ваше прізвище</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="Введіть ваше прізвище"
                                name="lastName"
                            />
                            <Form.Control.Feedback type="invalid">
                                Будь ласка введіть ваше прізвище
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Емейл адреса</Form.Label>
                            <Form.Control
                                name="email"
                                required
                                type="email"
                                placeholder="Введіть емейл адресу"
                            />
                            <Form.Control.Feedback type="invalid">
                                Будь ласка введіть емейл адресу
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control
                                name="password"
                                required
                                type="password"
                                placeholder="Пароль"
                                minLength={6}
                                onChange={onChange}
                            />
                            <Form.Control.Feedback type="invalid">
                                Будь ласка введіть пароль
                            </Form.Control.Feedback>
                            <Form.Text className="text-muted">
                                Пароль повинен складатися як мінімум з 6 символів!
                            </Form.Text>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPasswordRepeat">
                            <Form.Label>Повторіть пароль</Form.Label>
                            <Form.Control
                                name="confirmPassword"
                                required
                                type="password"
                                placeholder="Повторіть пароль"
                                minLength={6}
                                onChange={onChange}
                            />
                            <Form.Control.Feedback type="invalid">
                                Обидва паролі повинні співпадати
                            </Form.Control.Feedback>
                        </Form.Group>

                        <Row className="pb-2">
                            <Col>
                                У вас вже є акаунт?
                                <Link to={"/login"}> Логін </Link>
                            </Col>
                        </Row>

                        <Button type="submit">
                            <Spinner
                                as="span"
                                animation="border"
                                size="sm"
                                role="status"
                                aria-hidden="true"
                            />
                            Створити
                        </Button>
                        <Alert show={true} variant="danger">
                            Користувач з таким емейлом вже існує!
                        </Alert>
                        <Alert show={true} variant="info">
                            Користувача створено
                        </Alert>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default RegisterPage;

